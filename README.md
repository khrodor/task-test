# Fullstack programming task

This repository hosts the source code for fullstack programming task.

## Prerequisites
- [node 10.15.3](https://nodejs.org/ja/blog/release/v10.15.3/)
- [dotnet core 3.1 installed](https://dotnet.microsoft.com/download/dotnet-core/3.1)
- [yarn 1.16.0](https://github.com/yarnpkg/yarn/releases/tag/v1.16.0)
- [Visual Studio Community](https://visualstudio.microsoft.com/downloads/) or [Visual Studio Code](https://visualstudio.microsoft.com/downloads/) installed

## Debugging
- Open solution in Visual Studio
- Set `Api` and `UI` project as startup projects in Visual Studio
- Open cmd/powershell from `UI` folder and run `yarn run init`
- Start debugging in Visual Studio
  - Swagger and UI browser windows should be open

## Running UI tests
- Open cmd/powershell from `UI` folder and run `yarn run test` (this will start Karma test runner)

## Assignment

This repository contains a simplified example of a module consisting of an Angular front-end and a .NET Core back-end with a REST API and a SQL data store. The application is about administering select lists used by a case management system. To make things more simple, this version uses an in-memory database.

Currently the application will show a list of all select lists. Our customer has a requirement for editing select list's information. They need to edit the "Name" and "Description" fields. The pen icon should open up an edit view.

Your task is to implement an edit view in the UI and all necessary functionalities throughout the stack, so that the edited information ends up into the database succesfully. Keep an eye on the quality and maintainability of the code.
