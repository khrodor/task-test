// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
module.exports = function (config)
{
  config.set({
    basePath: "",
    // NOTE: 'parallel' must be the first framework in the list
    frameworks: ["parallel", "jasmine", "@angular-devkit/build-angular"],
    plugins: [
      require("karma-parallel"),
      require("karma-jasmine"),
      require("karma-chrome-launcher"),
      require("karma-jasmine-html-reporter"),
      require("karma-coverage-istanbul-reporter"),
      require("@angular-devkit/build-angular/plugins/karma")
    ],
    parallelOptions: {
      executors: 1, // Defaults to cpu-count - 1
      shardStrategy: "round-robin"
    },
    client: {
      jasmine: {
        random: false
      },
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    coverageIstanbulReporter: {
      dir: require("path").join(__dirname, "./coverage"),
      reports: ["html", "lcovonly", "text-summary"],
      fixWebpackSourcePaths: true
    },
    reporters: ["progress", "kjhtml"],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ["Chrome"],
    singleRun: false,
    browserNoActivityTimeout: 30000
  });
};
