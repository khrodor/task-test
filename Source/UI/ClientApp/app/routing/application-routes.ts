﻿import { ModuleWithProviders } from "@angular/core";
import { RouterModule } from "@angular/router";

import { HomePageComponent } from "../../components/home-page/home-page.component";

export const applicationRoutes: ModuleWithProviders = RouterModule.forRoot([
    {
        path: "", component: HomePageComponent
    }]);


