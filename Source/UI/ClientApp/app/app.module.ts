import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule } from "@angular/router";

import { HomePageComponent } from "../components/home-page/home-page.component";
import { MaterialModule } from "../shared/material.module";
import { AppComponent } from "./app.component";
import { applicationRoutes } from "./routing/application-routes";
import { EditSelectListComponent } from "ClientApp/components/home-page/edit-select-list/edit-select-list.component";

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        RouterModule,
        HttpClientModule,
        FlexLayoutModule,
        MaterialModule,
        FormsModule,
        applicationRoutes
    ],
    declarations: [AppComponent, HomePageComponent, EditSelectListComponent],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
