export class SelectList
{
        /// <summary>
        /// Select list unique identifier
        /// </summary>
        public id: string;

        /// <summary>
        /// Select list name
        /// </summary>
        public name: string;

        /// <summary>
        /// Select list description
        /// </summary>
        public  description: string;

        /// <summary>
        /// Select list technical value type
        /// </summary>
        public technicalValueType: string;

        /// <summary>
        /// Value for determining is select list disabled
        /// </summary>
        public isDisabled: boolean;
}