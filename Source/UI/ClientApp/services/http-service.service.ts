import { HttpHeaders } from "@angular/common/http";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";

@Injectable({
    providedIn: "root"
})
export class HttpService
{
    private serverUrl = "https://localhost:44357/";

    private httpOptions = {
        headers: this.httpHeaders
    };

    private get httpHeaders(): HttpHeaders
    {
        return new HttpHeaders({
            "content-Type": "application/json",
            "cache-Control": "no-cache, no-store",
            "pragma": "no-cache",
            accept: "application/json",
            "expires": "0"
        });
    }

    constructor(private httpClient: HttpClient) { }

    public get<T>(path: string): Observable<T>
    {
        return this.httpClient.get<T>(`${this.serverUrl}${path}`, this.httpOptions).pipe(
            catchError(err => throwError(err)));
    }

    public post<T>(path: string, body: object): Observable<T>
    {
        return this.httpClient.post<T>(`${this.serverUrl}${path}`,
            JSON.stringify(body),
            { headers: this.httpHeaders }).pipe(
                catchError(err => throwError(err)));
    }

    public put<T>(path: string, body: object): Observable<T>
    {
        return this.httpClient.put<T>(`${this.serverUrl}${path}`,
            JSON.stringify(body),
            { headers: this.httpHeaders }).pipe(
                catchError(err => throwError(err)));
    }
}




