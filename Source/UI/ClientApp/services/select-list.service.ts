import { Injectable } from "@angular/core";

import { SelectList } from "../models/select-list";
import { HttpService } from "./http-service.service";
import { Observable } from "rxjs";

@Injectable({
    providedIn: "root"
})
export class SelectListService
{
    constructor(private httpService: HttpService) {}

    public getSelectLists(): Observable<SelectList[]>
{
        return this.httpService.get<SelectList[]>("selectlist");
    }

    public updateSelectList(
        id: string,
        data: SelectList
    ): Observable<SelectList>
{
        return this.httpService.put<SelectList>(`selectlist/${id}`, data);
    }
}
