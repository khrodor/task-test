import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { FormsModule } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { By } from "@angular/platform-browser";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";

import { EditSelectListComponent } from "./edit-select-list.component";
import { MaterialModule } from "ClientApp/shared/material.module";
import { noop } from "lodash";

export class MatDialogRefMock
{
    public close(): void
{
        noop();
    }
}

describe("EditSelectListComponent", () =>
{
    let component: EditSelectListComponent;
    let fixture: ComponentFixture<EditSelectListComponent>;

    beforeEach(async(() =>
{
        TestBed.configureTestingModule({
            imports: [MaterialModule, NoopAnimationsModule, FormsModule],
            declarations: [EditSelectListComponent],
            providers: [
                { provide: MAT_DIALOG_DATA, useValue: {} },
                {
                    provide: MatDialogRef,
                    useClass: MatDialogRefMock
                }
            ]
        }).compileComponents();
    }));

    beforeEach(() =>
{
        fixture = TestBed.createComponent(EditSelectListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () =>
{
        expect(component).toBeTruthy();
    });

    it("should close when clicked cancel", async () =>
{
        await fixture.whenStable();
        fixture.detectChanges();

        spyOn(component.dialogRef, "close");

        const cancelButton = fixture.debugElement.query(
            By.css(".cancelButton")
        );
        cancelButton.nativeElement.click();

        expect(component.dialogRef.close).toHaveBeenCalledWith();
    });

    it("should close with model when clicked confirm", async () =>
{
        await fixture.whenStable();
        fixture.detectChanges();

        spyOn(component.dialogRef, "close");
        const confirmButton = fixture.debugElement.query(
            By.css(".confirmButton")
        );
        confirmButton.nativeElement.click();

        expect(component.dialogRef.close).toHaveBeenCalledWith(component.model);
    });
});
