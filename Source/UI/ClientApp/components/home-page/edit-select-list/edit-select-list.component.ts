import { Component, Inject, ViewEncapsulation } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";

import { SelectList } from "ClientApp/models/select-list";

@Component({
    selector: "app-edit-select-list",
    templateUrl: "./edit-select-list.component.html",
    styleUrls: ["./edit-select-list.component.scss"],
    encapsulation: ViewEncapsulation.Emulated
})
export class EditSelectListComponent
{
    public model: SelectList = new SelectList();

    constructor(
        public dialogRef: MatDialogRef<EditSelectListComponent>,
        @Inject(MAT_DIALOG_DATA) public data: SelectList
    )
{
        this.model = Object.assign({}, data);
    }

    public onNoClick(): void
{
        this.dialogRef.close();
    }
}
