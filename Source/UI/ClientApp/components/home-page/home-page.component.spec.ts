import { HttpClientModule } from "@angular/common/http";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { MatDialogRef } from "@angular/material/dialog";
import { By } from "@angular/platform-browser";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";

import { SelectList } from "../../models/select-list";
import { SelectListService } from "../../services/select-list.service";
import { MaterialModule } from "../../shared/material.module";
import { EditSelectListComponent } from "./edit-select-list/edit-select-list.component";
import { HomePageComponent } from "./home-page.component";
import { of } from "rxjs";
import { Observable } from "rxjs/internal/Observable";

export class SelectListServiceMock
{
    public getSelectLists(): Observable<SelectList[]>
{
        const items: SelectList[] = [
            {
                id: "001",
                name: "list 1",
                description: "list 1 description",
                technicalValueType: "boolean",
                isDisabled: true
            },
            {
                id: "002",
                name: "list 2",
                description: "list 2 description",
                technicalValueType: "boolean",
                isDisabled: true
            },
            {
                id: "003",
                name: "list 3",
                description: "list 3 description",
                technicalValueType: "boolean",
                isDisabled: true
            }
        ];

        return of(items);
    }

    public updateSelectList(
        id: string,
        data: SelectList
    ): Observable<SelectList>
{
        return of(data);
    }
}

const getAfterClosedMock = (
    result: object
): MatDialogRef<EditSelectListComponent, SelectList> =>
{
    return {
        afterClosed: (): Observable<object> => of(result)
    } as MatDialogRef<EditSelectListComponent, SelectList>;
};

describe("HomePageComponent", () =>
{
    let component: HomePageComponent;
    let fixture: ComponentFixture<HomePageComponent>;

    beforeEach(async(() =>
{
        TestBed.configureTestingModule({
            imports: [MaterialModule, HttpClientModule, NoopAnimationsModule],
            declarations: [HomePageComponent],
            providers: [
                {
                    provide: SelectListService,
                    useClass: SelectListServiceMock
                }
            ]
        }).compileComponents();
    }));

    beforeEach(() =>
{
        fixture = TestBed.createComponent(HomePageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () =>
{
        expect(component).toBeTruthy();
    });

    it("should contain table and table should contains items", async () =>
{
        await fixture.whenStable();
        fixture.detectChanges();

        const tableElement = fixture.debugElement.queryAll(By.css("table"));
        const rowElements = fixture.debugElement.queryAll(By.css("tr"));

        expect(tableElement).toBeTruthy();
        expect(rowElements.length).toBe(4);
    });

    it("should show edit modal when edit button is clicked", async () =>
{
        await fixture.whenStable();
        fixture.detectChanges();

        spyOn(component.dialog, "open").and.returnValue(
            getAfterClosedMock(null)
        );

        const button = fixture.nativeElement.querySelector(
            "button[aria-label=\"edit item\"]"
        );
        button.click();

        fixture.detectChanges();

        expect(component.dialog.open).toHaveBeenCalled();
    });

    it("should not update select list when edit dialog not return result", async () =>
{
        await fixture.whenStable();
        fixture.detectChanges();

        spyOn(component.dialog, "open").and.returnValue(
            getAfterClosedMock(null)
        );
        spyOn(component.selectListService, "updateSelectList");

        const button = fixture.nativeElement.querySelector(
            "button[aria-label=\"edit item\"]"
        );
        button.click();

        expect(
            component.selectListService.updateSelectList
        ).toHaveBeenCalledTimes(0);
    });

    it("should modify list element when update completed", async () =>
{
        await fixture.whenStable();
        fixture.detectChanges();

        const updatedResult = Object.assign({}, component.lists[0]);
        updatedResult.name = "changed name";
        spyOn(component.dialog, "open").and.returnValue(
            getAfterClosedMock(updatedResult)
        );
        const button = fixture.nativeElement.querySelector(
            "button[aria-label=\"edit item\"]"
        );
        button.click();

        expect(
            component.lists.filter(l => l.id === updatedResult.id)[0].name
        ).toBe(updatedResult.name);
    });

    it("should update select list when edit model return result", async () =>
{
        await fixture.whenStable();
        fixture.detectChanges();

        spyOn(component.dialog, "open").and.returnValue(
            getAfterClosedMock(new SelectList())
        );
        spyOn(component.selectListService, "updateSelectList");

        const button = fixture.nativeElement.querySelector(
            "button[aria-label=\"edit item\"]"
        );
        button.click();

        expect(
            component.selectListService.updateSelectList
        ).toHaveBeenCalledTimes(1);
    });
});
