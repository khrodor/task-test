import { OnInit } from "@angular/core";
import { Component, ViewEncapsulation } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";

import { SelectList } from "../../models/select-list";
import { SelectListService } from "../../services/select-list.service";
import { EditSelectListComponent } from "./edit-select-list/edit-select-list.component";
import { CONSTANTS } from "ClientApp/shared/constants";

@Component({
    selector: "app-home-page",
    templateUrl: "./home-page.component.html",
    styleUrls: ["./home-page.component.scss"],
    encapsulation: ViewEncapsulation.Emulated
})
export class HomePageComponent implements OnInit
{
    public displayedColumns: string[] = [
        "name",
        "description",
        "technicalValueType",
        "isDisabled",
        "edit"
    ];

    public lists: SelectList[];

    constructor(
        public selectListService: SelectListService,
        public dialog: MatDialog
    ) {}

    public async ngOnInit(): Promise<void>
{
        this.lists = await this.selectListService.getSelectLists().toPromise();
    }

    public editSelectList(element: SelectList): void
{
        const copyOfElement = Object.assign({}, element);
        const dialog = this.dialog.open<
            EditSelectListComponent,
            SelectList,
            SelectList
        >(EditSelectListComponent, {
            data: copyOfElement,
            width: CONSTANTS.defaultModalWidth
        });
        dialog
            .afterClosed()
            .subscribe(result => this.updateSelectList(result));
    }

    private updateSelectList(result: SelectList): void
{
        if (result == null)
{
            return;
        }
        this.selectListService
            .updateSelectList(result.id, result)
            .subscribe(updatedEntity =>
{
                this.lists
                    .filter(el => el.id === updatedEntity.id)
                    .map(el => Object.assign(el, updatedEntity));
            });
    }
}
