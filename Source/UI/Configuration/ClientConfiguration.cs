﻿namespace Fullstack.UI.Configuration
{
    public class ClientConfiguration
    {
        public string ApiBaseUrl { get; set; } = default!;
    }
}