﻿using Fullstack.UI.Configuration;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Fullstack.UI.Controllers
{
    public class ClientConfigurationController : Controller
    {
        private readonly ClientConfiguration clientConfiguration;

        public ClientConfigurationController(IOptions<ClientConfiguration> clientConfiguration)
        {
            this.clientConfiguration = clientConfiguration.Value;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return Json(clientConfiguration);
        }
    }
}