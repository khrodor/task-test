﻿using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace Fullstack.UI.Controllers
{
    public class HomeController : Controller
    {
        private readonly IWebHostEnvironment webHostingEnvironment;

        public HomeController(IWebHostEnvironment webHostingEnvironment)
        {
            this.webHostingEnvironment = webHostingEnvironment ?? throw new ArgumentNullException(nameof(webHostingEnvironment));
        }

        public IActionResult Index()
        {
            string webRoot = webHostingEnvironment.WebRootPath;
            string indexFileContent = System.IO.File.ReadAllText(Path.Combine(webRoot, "dist\\index.html"));

            ViewBag.Scripts = indexFileContent.Replace("src=\"", "src=\"dist/").Replace("href=\"", "href=\"dist/");

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}