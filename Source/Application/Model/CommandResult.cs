﻿using System;

namespace Fullstack.SelectList.Application.Model
{
    public class CommandResult
    {
        public bool IsSuccess { get; private set; }
        public string ErrorMessage { get; private set; } = default!;
        public Exception? Exception { get; private set; }
        private CommandResult()
        {

        }

        public static CommandResult Success() => new CommandResult
        {
            IsSuccess = true
        };

        public static CommandResult Error(string message) => new CommandResult
        {
            IsSuccess = false,
            ErrorMessage = message
        };

        public static CommandResult Error(Exception exception) => new CommandResult
        {
            IsSuccess = false,
            Exception = exception
        };
    }
}
