﻿namespace Fullstack.SelectList.Application.Model
{
    public enum TechnicalValueType
    {
        Boolean,
        Number
    }
}
