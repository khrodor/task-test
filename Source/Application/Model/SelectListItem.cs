﻿using System;

namespace Fullstack.SelectList.Application.Model
{
    public class SelectListItem
    {
        public SelectListItem(Guid id, string technicalName, object technicalValue, int order, bool isDisabled)
        {
            Id = id;
            TechnicalName = technicalName;
            TechnicalValue = technicalValue;
            Order = order;
            IsDisabled = isDisabled;
        }

        public Guid Id { get; }
        public string TechnicalName { get; }
        public object TechnicalValue { get; }
        public int Order { get; }
        public bool IsDisabled { get; }


    }
}
