﻿using System;
using System.Collections.Generic;

namespace Fullstack.SelectList.Application.Model
{
    public class SelectList
    {
        public SelectList(Guid id, string name, string description, TechnicalValueType technicalValueType, bool isDisabled,
            IReadOnlyCollection<SelectListItem> items)
        {
            Id = id;
            Name = name;
            Description = description;
            TechnicalValueType = technicalValueType;
            IsDisabled = isDisabled;
            Items = items;
        }

        public Guid Id { get; }
        public string Name { get; }
        public string Description { get; }
        public TechnicalValueType TechnicalValueType { get; }
        public bool IsDisabled { get; }
        public IReadOnlyCollection<SelectListItem> Items { get; }
    }
}
