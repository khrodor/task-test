﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Fullstack.SelectList.Application.Exceptions
{
    public class EntityNotExistsException : Exception
    {
        public EntityNotExistsException()
        {
        }

        public EntityNotExistsException(string? message) : base(message)
        {
        }

        public EntityNotExistsException(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        protected EntityNotExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
