﻿using Autofac;
using Fullstack.SelectList.Application.Commands.UpdateSelectList;
using Fullstack.SelectList.Application.Queries.GetSelectLists;
using Fullstack.SelectList.Application.Queries.GetSingleSelectList;

namespace Fullstack.SelectList.Application
{
    public class ApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            RegisterCommandValidators(builder);
            RegisterRequestHandlers(builder);
            RegisterMappers(builder);
        }

        private static void RegisterRequestHandlers(ContainerBuilder builder)
        {
            builder.RegisterType<GetSelectListsQueryHandler>().AsImplementedInterfaces();
            builder.RegisterType<GetSingleSelectListQueryHandler>().AsImplementedInterfaces();
            builder.RegisterType<UpdateSelectListCommandHandler>().AsImplementedInterfaces();
        }

        private static void RegisterCommandValidators(ContainerBuilder builder)
        {

        }

        private static void RegisterMappers(ContainerBuilder builder)
        {

        }
    }
}