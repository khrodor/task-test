﻿using System.Threading.Tasks;

namespace Fullstack.SelectList.Application.Validation
{
    internal interface IValidator<in T> : FluentValidation.IValidator<T>
    {
        void ValidateAndThrow(T instance);
        Task ValidateAndThrowAsync(T instance);
    }
}
