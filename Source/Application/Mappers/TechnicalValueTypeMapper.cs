﻿using System;
using Fullstack.SelectList.Application.Model;
using DataModel = Fullstack.SelectList.DataStore.Model;

namespace Fullstack.SelectList.Application.Mappers
{
    internal class TechnicalValueTypeMapper
    {
        private const string exceptionMessage = "Invalid technical value type";

        public static TechnicalValueType MapOut(DataModel.TechnicalValueType valueType)
        {
            switch (valueType)
            {
                case DataModel.TechnicalValueType.Boolean:
                    return TechnicalValueType.Boolean;

                case DataModel.TechnicalValueType.Number:
                    return TechnicalValueType.Number;

                default:
                    throw new ArgumentException(exceptionMessage);
            }
        }

        public static DataModel.TechnicalValueType MapIn(TechnicalValueType valueType)
        {
            switch (valueType)
            {
                case TechnicalValueType.Boolean:
                    return DataModel.TechnicalValueType.Boolean;

                case TechnicalValueType.Number:
                    return DataModel.TechnicalValueType.Number;

                default:
                    throw new ArgumentException(exceptionMessage);
            }
        }
    }
}
