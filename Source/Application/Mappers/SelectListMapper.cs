﻿using DataModel = Fullstack.SelectList.DataStore.Model;

namespace Fullstack.SelectList.Application.Mappers
{
    internal class SelectListMapper
    {
        public static Model.SelectList Map(DataModel.SelectList selectList)
        {
            return new Model.SelectList(selectList.Id, selectList.Name, selectList.Description,
                TechnicalValueTypeMapper.MapOut(selectList.TechnicalValueType), selectList.IsDisabled,
                SelectListItemMapper.MapItems(selectList.Items, selectList.TechnicalValueType));
        }
    }
}