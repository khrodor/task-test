﻿using System.Collections.Generic;
using System.Linq;
using Fullstack.SelectList.Application.Model;
using DataModel = Fullstack.SelectList.DataStore.Model;

namespace Fullstack.SelectList.Application.Mappers
{
    internal class SelectListItemMapper
    {
        public static IReadOnlyCollection<SelectListItem> MapItems(ICollection<DataModel.SelectListItem> items, DataModel.TechnicalValueType selectListTechnicalValueType)
        {
            return items?.Select(item => MapItem(item, selectListTechnicalValueType)).ToList() ?? new List<SelectListItem>();

        }

        private static SelectListItem MapItem(DataModel.SelectListItem item, DataModel.TechnicalValueType selectListTechnicalValueType)
        {
            return new SelectListItem(item.Id, item.TechnicalName,
                MapTechnicalValue(item.TechnicalValue, selectListTechnicalValueType), item.Order, item.IsDisabled);
        }

        private static object MapTechnicalValue(string itemTechnicalValue, DataModel.TechnicalValueType selectListTechnicalValueType)
        {
            switch (selectListTechnicalValueType)
            {
                case DataModel.TechnicalValueType.Boolean:
                    return bool.Parse(itemTechnicalValue);

                case DataModel.TechnicalValueType.Number:
                    return int.Parse(itemTechnicalValue);

                default:
                    return itemTechnicalValue;
            }
        }
    }
}