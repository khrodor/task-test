﻿using System;
using MediatR;

namespace Fullstack.SelectList.Application.Queries.GetSingleSelectList
{
    public class GetSingleSelectListQuery: IRequest<Model.SelectList>
    {
        public GetSingleSelectListQuery(Guid id)
        {
            Id = id;
        }
        public Guid Id { get; }
    }
}
