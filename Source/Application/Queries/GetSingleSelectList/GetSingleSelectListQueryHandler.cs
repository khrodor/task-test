﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Fullstack.SelectList.Application.Mappers;
using Fullstack.SelectList.DataStore;
using Fullstack.SelectList.DataStore.Queries;
using MediatR;

namespace Fullstack.SelectList.Application.Queries.GetSingleSelectList
{

    internal class GetSingleSelectListQueryHandler : IRequestHandler<GetSingleSelectListQuery, Model.SelectList>
    {
        private readonly ISelectListContext dbContext;

        public GetSingleSelectListQueryHandler(ISelectListContext dbContext)
        {
            this.dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public async Task<Model.SelectList> Handle(GetSingleSelectListQuery request, CancellationToken cancellationToken)
        {
            var query = new SingleSelectListQuery(dbContext.SelectLists);
            return SelectListMapper.Map(await query.Execute(request.Id));
        }
    }
}
