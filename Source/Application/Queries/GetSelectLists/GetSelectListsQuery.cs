﻿using System.Collections.Generic;
using MediatR;

namespace Fullstack.SelectList.Application.Queries.GetSelectLists
{
    public class GetSelectListsQuery : IRequest<IReadOnlyCollection<Model.SelectList>>
    {
    }
}
