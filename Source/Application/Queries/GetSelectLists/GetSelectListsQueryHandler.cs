﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Fullstack.SelectList.Application.Mappers;
using Fullstack.SelectList.DataStore;
using Fullstack.SelectList.DataStore.Queries;
using MediatR;
using DataModel = Fullstack.SelectList.DataStore.Model;

namespace Fullstack.SelectList.Application.Queries.GetSelectLists
{
    internal class GetSelectListsQueryHandler : IRequestHandler<GetSelectListsQuery, IReadOnlyCollection<Model.SelectList>>
    {
        private readonly ISelectListContext dbContext;

        public GetSelectListsQueryHandler(ISelectListContext dbContext)
        {
            this.dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public async Task<IReadOnlyCollection<Model.SelectList>> Handle(GetSelectListsQuery request, CancellationToken cancellationToken)
        {
            SelectListsQuery query = new SelectListsQuery(dbContext.SelectLists);
            IReadOnlyCollection<DataModel.SelectList> dataStoreSelectLists = await query.Execute();
            IReadOnlyCollection<Model.SelectList> resultList = dataStoreSelectLists.Select(SelectListMapper.Map).ToList();
            return resultList;
        }
    }
}
