﻿using System;
using Fullstack.SelectList.Application.Model;
using MediatR;

namespace Fullstack.SelectList.Application.Commands.UpdateSelectList
{
    public class UpdateSelectListCommand : IRequest<CommandResult>
    {
        public UpdateSelectListCommand(Guid id, string name, string description)
        {
            Id = id;
            Name = name;
            Description = description;
        }

        public Guid Id { get; }
        public string Name { get; }
        public string Description { get; }
    }
}
