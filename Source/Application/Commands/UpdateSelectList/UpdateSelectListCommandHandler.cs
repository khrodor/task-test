﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Fullstack.SelectList.Application.Exceptions;
using Fullstack.SelectList.Application.Helpers;
using Fullstack.SelectList.Application.Model;
using Fullstack.SelectList.DataStore;
using MediatR;

namespace Fullstack.SelectList.Application.Commands.UpdateSelectList
{
    internal class UpdateSelectListCommandHandler : IRequestHandler<UpdateSelectListCommand, CommandResult>
    {
        private readonly ISelectListContext dbContext;

        public UpdateSelectListCommandHandler(ISelectListContext dbContext)
        {
            this.dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public async Task<CommandResult> Handle(UpdateSelectListCommand request, CancellationToken cancellationToken)
        {
            var entity = await dbContext
                .SelectLists
                .FindAsync(request.Id);
            if (entity == null)
            {
                return CommandResult.Error(new EntityNotExistsException());
            }

            ObjectModifierHelper.ModifyPropertyIfChanged(entity, nameof(entity.Name), request.Name);
            ObjectModifierHelper.ModifyPropertyIfChanged(entity, nameof(entity.Description), request.Description);

            try
            {
                await dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return CommandResult.Error(ex);
            }
            return CommandResult.Success();
        }
    }
}
