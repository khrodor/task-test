﻿using System;

namespace Fullstack.SelectList.Application.Helpers
{
    internal class ObjectModifierHelper
    {
        public static void ModifyPropertyIfChanged<T>(T instance, string propertyName, object newValue)
        {
            var property = typeof(T).GetProperty(propertyName) ?? throw new Exception("Property does not exists");

            var currentValue = property.GetValue(instance);
            if (!Equals(currentValue, newValue))
            {
                property.SetValue(instance, newValue);
            }
        }
    }
}
