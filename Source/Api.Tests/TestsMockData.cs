﻿using System;
using System.Collections.Generic;
using System.Linq;
using ApiModel = Fullstack.SelectList.Api.Model;
using DataModel = Fullstack.SelectList.DataStore.Model;

namespace Fullstack.SelectList.Api.Tests
{
    internal class TestsMockData
    {
        internal static IReadOnlyCollection<DataModel.SelectList> MockSelectLists()
        {
            return new List<DataModel.SelectList>
            {
                new DataModel.SelectList
                {
                    Id = Guid.Parse("B1000000-0000-0000-0000-000000000001"),
                    Name = "List 1",
                    Description = "List 1 description",
                    TechnicalValueType = DataModel.TechnicalValueType.Number, IsDisabled = false,
                },
                new DataModel.SelectList
                {
                    Id = Guid.Parse("B1000000-0000-0000-0000-000000000002"),
                    Name = "List 2",
                    Description = "List 2 description",
                    TechnicalValueType = DataModel.TechnicalValueType.Boolean, IsDisabled = false,
                }
            };
        }

        internal static IReadOnlyCollection<DataModel.SelectListItem> MockSelectListItems()
        {
            return new List<DataModel.SelectListItem>
            {
                new DataModel.SelectListItem
                {
                    Id = Guid.Parse("B2000000-0000-0000-0000-000000000001"), TechnicalName = "name 1",
                    TechnicalValue = "value 1",
                    Order = 1,
                    IsDisabled = false,
                    SelectListId = Guid.Parse("B1000000-0000-0000-0000-000000000001")
                },
                new DataModel.SelectListItem
                {
                    Id = Guid.Parse("B2000000-0000-0000-0000-000000000002"), TechnicalName = "name 2",
                    TechnicalValue = "value 2",
                    Order = 2,
                    IsDisabled = false,
                    SelectListId = Guid.Parse("B1000000-0000-0000-0000-000000000001")
                }
            };
        }

        internal static IReadOnlyCollection<ApiModel.SelectList> ExpectedSelectListResult(List<Guid> selectListsIds)
        {
            return ExpectedSelectListResult().Where(x => selectListsIds.Contains(x.Id)).ToList();
        }

        internal static IReadOnlyCollection<ApiModel.SelectList> ExpectedSelectListResult()
        {
            return new List<ApiModel.SelectList>
            {
                new ApiModel.SelectList 
                { 
                    Id = Guid.Parse("B1000000-0000-0000-0000-000000000001"),
                    Name = "List 1",
                    Description = "List 1 description",
                    TechnicalValueType = ApiModel.TechnicalValueType.Number,
                    IsDisabled = false,
                },
                new ApiModel.SelectList 
                { 
                    Id = Guid.Parse("B1000000-0000-0000-0000-000000000002"),
                    Name = "List 2",
                    Description = "List 2 description",
                    TechnicalValueType = ApiModel.TechnicalValueType.Boolean,
                    IsDisabled = false
                }
            };
        }
    }
}
