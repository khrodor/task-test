﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Xunit;
using ApiModel = Fullstack.SelectList.Api.Model;

namespace Fullstack.SelectList.Api.Tests.SelectLists
{
    public class GetSelectListsTests : BaseTests
    {
        private const string Url = "/";

        public GetSelectListsTests() : base(TestsConstants.SelectListController)
        {
        }

        [Fact]
        public async Task SelectListsReturn()
        {
            await MockDbContextAndRunTest(async () =>
            {
                IReadOnlyCollection<ApiModel.SelectList> returnedSelectLists = await SendGetRequest<IReadOnlyCollection<ApiModel.SelectList>>(Url);

                IEnumerable<ApiModel.SelectList> expectedSelectLists = TestsMockData.ExpectedSelectListResult(new List<Guid>
                {
                    Guid.Parse("B1000000-0000-0000-0000-000000000001"),
                    Guid.Parse("B1000000-0000-0000-0000-000000000002")
                });

                returnedSelectLists.Should().NotBeNull();
                returnedSelectLists.Should().BeEquivalentTo(expectedSelectLists);
            });
        }
    }
}
