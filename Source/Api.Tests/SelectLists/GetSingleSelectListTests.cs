﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Xunit;
using ApiModel = Fullstack.SelectList.Api.Model;

namespace Fullstack.SelectList.Api.Tests.SelectLists
{
    public class GetSingleSelectListTests : BaseTests
    {
        private const string Url = "/";

        public GetSingleSelectListTests() : base(TestsConstants.SelectListController)
        {
        }

        [Fact]
        public async Task SingleSelectListReturn()
        {
            await MockDbContextAndRunTest(async () =>
            {
                string selectListId = "B1000000-0000-0000-0000-000000000001";
                ApiModel.SelectList returnedSelectList = await SendGetRequest<ApiModel.SelectList>(Url + selectListId);

                IEnumerable<ApiModel.SelectList> expectedSelectLists = TestsMockData.ExpectedSelectListResult(new List<Guid>
                {
                    Guid.Parse("B1000000-0000-0000-0000-000000000001")
                });

                returnedSelectList.Should().NotBeNull();
                returnedSelectList.Should().BeEquivalentTo(expectedSelectLists.FirstOrDefault());
            });
        }
    }
}
