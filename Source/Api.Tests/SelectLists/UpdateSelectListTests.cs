﻿using System.Threading.Tasks;
using FluentAssertions;
using Xunit;

namespace Fullstack.SelectList.Api.Tests.SelectLists
{
    public class UpdateSelectListTests : BaseTests
    {
        private const string Url = "/B1000000-0000-0000-0000-000000000001";

        public UpdateSelectListTests() : base(TestsConstants.SelectListController)
        {
        }

        [Fact]
        public async Task SelectListsReturn()
        {
            await MockDbContextAndRunTest(async () =>
            {
                var command = new Commands.UpdateSelectListCommand
                {
                    Name = "foo",
                    Description = "bar"
                };
                Api.Model.SelectList returnedSelectList = await SendPutRequest<Commands.UpdateSelectListCommand, Api.Model.SelectList>(Url, command);

                returnedSelectList.Should().NotBeNull();
                returnedSelectList.Name.Should().Be(command.Name);
                returnedSelectList.Description.Should().Be(command.Description);
            });
        }
    }
}
