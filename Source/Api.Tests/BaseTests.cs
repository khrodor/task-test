﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Fullstack.SelectList.DataStore;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using DataModel = Fullstack.SelectList.DataStore.Model;

namespace Fullstack.SelectList.Api.Tests
{
    public abstract class BaseTests : IDisposable
    {
        private readonly WebApplicationFactory<Startup> webApplicationFactory;
        private readonly HttpClient testServerClient;
        private readonly string controllerName;

        protected BaseTests(string controllerName)
        {
            if (string.IsNullOrWhiteSpace(controllerName))
            {
                throw new ArgumentException(nameof(controllerName));
            }

            this.controllerName = controllerName;

            webApplicationFactory = CreateWebApplicationFactory();
            testServerClient = CreateTestServerClient();
        }

        public void Dispose()
        {
            testServerClient.Dispose();
        }

        public string GetFullUrl(string url)
        {
            return controllerName + "/" + url.TrimStart('/');
        }

        public async Task<TResult> SendGetRequest<TResult>(string url)
        {
            HttpRequestMessage message = GetHttpRequestMessageWithHeaders(HttpMethod.Get, url);
            HttpResponseMessage response = await SendRequest(message);
            return await GetResponseContent<TResult>(response) ?? default!;
        }

        public async Task<TResult> SendPostRequest<TCommand, TResult>(string url, TCommand command)
        {
            return await SendRequestWithContent<TCommand, TResult>(HttpMethod.Post, url, command);
        }

        public async Task<TResult> SendPutRequest<TCommand, TResult>(string url, TCommand command)
        {
            return await SendRequestWithContent<TCommand, TResult>(HttpMethod.Put, url, command);
        }

        public async Task<string?> SendDeleteRequest(string url)
        {
            HttpRequestMessage message = GetHttpRequestMessageWithHeaders(HttpMethod.Delete, url);
            HttpResponseMessage response = await SendRequest(message);
            return await GetResponseContent<string?>(response);
        }

        public async Task<TResult> SendRequestWithContent<TCommand, TResult>(HttpMethod httpMethod, string url, TCommand command)
        {
            string json = JsonConvert.SerializeObject(command);
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

            HttpRequestMessage message = GetHttpRequestMessageWithHeaders(httpMethod, url);
            message.Content = httpContent;

            HttpResponseMessage response = await SendRequest(message);
            TResult result = await GetResponseContent<TResult>(response);
            return result;
        }

        public async Task MockDbContextAndRunTest(Func<Task> action, List<DataModel.SelectList>? selectLists = null,
            List<DataModel.SelectListItem>? selectListItems = null)
        {
            ISelectListContext selectListContext = GetDbContext();
            ClearDbContext(selectListContext);
            MockDbContext(selectListContext, selectLists, selectListItems);

            await action();
        }

        protected ISelectListContext GetDbContext()
        {
            ISelectListContext? selectListContext = webApplicationFactory.Server.Services.GetService(typeof(ISelectListContext)) as ISelectListContext;

            if (selectListContext == null)
            {
                throw new ArgumentNullException(nameof(selectListContext));
            }

            return selectListContext;
        }

        private void ClearDbContext(ISelectListContext selectListContext)
        {
            selectListContext.SelectLists.RemoveRange(selectListContext.SelectLists);
            selectListContext.SelectListItems.RemoveRange(selectListContext.SelectListItems);

            selectListContext.SaveChanges();
        }

        private void MockDbContext(ISelectListContext selectListContext,
            IReadOnlyCollection<DataModel.SelectList>? selectLists,
            IReadOnlyCollection<DataModel.SelectListItem>? selectListItems)
        {
            selectLists ??= TestsMockData.MockSelectLists();
            selectListItems ??= TestsMockData.MockSelectListItems();

            selectListContext.SelectLists.AddRange(selectLists);
            selectListContext.SelectListItems.AddRange(selectListItems);

            selectListContext.SaveChanges();
        }

        private HttpRequestMessage GetHttpRequestMessageWithHeaders(HttpMethod httpMethod, string url)
        {
            var message = new HttpRequestMessage(httpMethod, GetFullUrl(url));

            message.Headers.Add("correlation-id", Guid.Empty.ToString("D"));
            message.Headers.Add("caller-id", Guid.Empty.ToString("D"));

            return message;
        }

        private WebApplicationFactory<Startup> CreateWebApplicationFactory()
        {
            var factory = new WebApplicationFactory<Startup>().WithWebHostBuilder(builder =>
            {
                builder.ConfigureTestServices(services =>
                {
                    var selectListsContextOptions = new DbContextOptionsBuilder<SelectListContext>()
                        .UseInMemoryDatabase($"SelectListsDatabase_{Guid.NewGuid()}")
                        .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                        .Options;

                    services.AddSingleton(selectListsContextOptions);
                });
            });

            return factory;
        }

        private HttpClient CreateTestServerClient()
        {
            HttpClient client = webApplicationFactory.CreateClient(new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = false
            });

            return client;
        }

        private async Task<HttpResponseMessage> SendRequest(HttpRequestMessage request)
        {
            HttpResponseMessage response = await testServerClient.SendAsync(request);
            return response;
        }

        private async Task<T> GetResponseContent<T>(HttpResponseMessage response)
        {
            string result = await response.Content.ReadAsStringAsync();

            if (string.IsNullOrWhiteSpace(result))
            {
                return default!;
            }

            if (typeof(T) == typeof(string))
            {
                return (T) (object) result;
            }

            return JsonConvert.DeserializeObject<T>(result);
        }
    }
}
