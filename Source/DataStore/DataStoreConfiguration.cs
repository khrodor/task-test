﻿using Fullstack.SelectList.Configuration;

namespace Fullstack.SelectList.DataStore
{
    public class DataStoreConfiguration : ConfigurationBase
    {
        public string ConnectionString { get; set; } = default!;
    }
}
