﻿namespace Fullstack.SelectList.DataStore.Model
{
    public enum TechnicalValueType
    {
        Boolean,
        Number
    }
}
