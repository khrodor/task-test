﻿using System;
using System.Collections.Generic;

namespace Fullstack.SelectList.DataStore.Model
{
    public class SelectList
    {
        public Guid Id { get; set; }

        public string Name { get; set; } = default!;
        public string Description { get; set; } = default!;
        public TechnicalValueType TechnicalValueType { get; set; }
        public bool IsDisabled { get; set; } = default!;
        public virtual ICollection<SelectListItem> Items { get; set; } = default!;
    }
}
