﻿using System;

namespace Fullstack.SelectList.DataStore.Model
{
    public class SelectListItem
    {
        public Guid Id { get; set; }
        public string TechnicalName { get; set; } = default!;
        public string TechnicalValue { get; set; } = default!;
        public int Order { get; set; }
        public bool IsDisabled { get; set; }
        public Guid SelectListId { get; set; }
    }
}
