﻿using System;
using Fullstack.SelectList.DataStore.Model;

namespace Fullstack.SelectList.DataStore
{
    public class DataStoreInitializer
    {
        public static void Init(ISelectListContext context)
        {
            AddSelectLists(context);
            AddSelectListItems(context);
            context.SaveChanges();
        }

        private static void AddSelectLists(ISelectListContext context)
        {
            var list1 = new Model.SelectList
            {
                Id = Guid.Parse("B1000000-0000-0000-0000-000000000001"),
                Name = "List 1",
                Description = "List 1 description",
                TechnicalValueType = TechnicalValueType.Number,
                IsDisabled = false,
            };

            var list2 = new Model.SelectList
            {
                Id = Guid.Parse("B1000000-0000-0000-0000-000000000002"),
                Name = "List 2",
                Description = "List 2 description",
                TechnicalValueType = TechnicalValueType.Boolean,
                IsDisabled = false,
            };

            var list3 = new Model.SelectList
            {
                Id = Guid.Parse("B1000000-0000-0000-0000-000000000003"),
                Name = "List 3",
                Description = "List 3 description",
                TechnicalValueType = TechnicalValueType.Boolean,
                IsDisabled = false,
            };

            var list4 = new Model.SelectList
            {
                Id = Guid.Parse("B1000000-0000-0000-0000-000000000004"),
                Name = "List 4",
                Description = "List 4 description",
                TechnicalValueType = TechnicalValueType.Boolean,
                IsDisabled = false,
            };

            context.SelectLists.Add(list1);
            context.SelectLists.Add(list2);
            context.SelectLists.Add(list3);
            context.SelectLists.Add(list4);
        }

        private static void AddSelectListItems(ISelectListContext context)
        {
            var item1 = new SelectListItem
            {
                Id = Guid.Parse("B2000000-0000-0000-0000-000000000001"),
                TechnicalName = "name 1",
                TechnicalValue = "value 1",
                Order = 1,
                IsDisabled = false,
                SelectListId = Guid.Parse("B1000000-0000-0000-0000-000000000001")
            };

            var item2 = new SelectListItem
            {
                Id = Guid.Parse("B2000000-0000-0000-0000-000000000002"),
                TechnicalName = "name 2",
                TechnicalValue = "value 2",
                Order = 2,
                IsDisabled = false,
                SelectListId = Guid.Parse("B1000000-0000-0000-0000-000000000001")
            };

            context.SelectListItems.Add(item1);
            context.SelectListItems.Add(item2);
        }
    }
}
