﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Fullstack.SelectList.DataStore.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SelectLists",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TechnicalValueType = table.Column<int>(nullable: false),
                    IsDisabled = table.Column<bool>(nullable: false),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SelectLists", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SelectListItems",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TechnicalName = table.Column<string>(nullable: false),
                    TechnicalValue = table.Column<string>(nullable: false),
                    Order = table.Column<int>(nullable: false),
                    IsDisabled = table.Column<bool>(nullable: false),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: false),
                    SelectListId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SelectListItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SelectListItems_SelectLists_SelectListId",
                        column: x => x.SelectListId,
                        principalTable: "SelectLists",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SelectListTranslations",
                columns: table => new
                {
                    LanguageCode = table.Column<string>(nullable: false),
                    SelectListId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SelectListTranslations", x => new { x.SelectListId, x.LanguageCode });
                    table.ForeignKey(
                        name: "FK_SelectListTranslations_SelectLists_SelectListId",
                        column: x => x.SelectListId,
                        principalTable: "SelectLists",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SelectListItemTranslations",
                columns: table => new
                {
                    LanguageCode = table.Column<string>(nullable: false),
                    SelectListItemId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SelectListItemTranslations", x => new { x.SelectListItemId, x.LanguageCode });
                    table.ForeignKey(
                        name: "FK_SelectListItemTranslations_SelectListItems_SelectListItemId",
                        column: x => x.SelectListItemId,
                        principalTable: "SelectListItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SelectListItems_SelectListId",
                table: "SelectListItems",
                column: "SelectListId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SelectListItemTranslations");

            migrationBuilder.DropTable(
                name: "SelectListTranslations");

            migrationBuilder.DropTable(
                name: "SelectListItems");

            migrationBuilder.DropTable(
                name: "SelectLists");
        }
    }
}
