﻿using System.IO;
using Fullstack.SelectList.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Fullstack.SelectList.DataStore.Factories
{
    public class SelectListContextFactory : IDesignTimeDbContextFactory<SelectListContext>
    {
        public SelectListContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            DataStoreConfiguration dataStoreConfiguration = configuration.GetSection("SelectListApi:DataStore").Initialize<DataStoreConfiguration>();
            dataStoreConfiguration.TryValidateObject();

            var optionsBuilder = new DbContextOptionsBuilder<SelectListContext>();
            optionsBuilder.UseSqlServer(dataStoreConfiguration.ConnectionString);
            return new SelectListContext(optionsBuilder.Options);
        }
    }
}
