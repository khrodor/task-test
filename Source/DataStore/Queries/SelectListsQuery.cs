﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Fullstack.SelectList.DataStore.Queries
{
    public class SelectListsQuery
    {
        private readonly IQueryable<Model.SelectList> selectListsDb;

        public SelectListsQuery(IQueryable<Model.SelectList> selectListsDb)
        {
            this.selectListsDb = selectListsDb;
        }

        public async Task<IReadOnlyCollection<Model.SelectList>> Execute()
        {
            List<Model.SelectList> selectListsData = await selectListsDb
                .ToListAsync();

            return selectListsData;
        }
    }
}
