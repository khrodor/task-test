﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Fullstack.SelectList.DataStore.Queries
{
    public class SingleSelectListQuery
    {
        private readonly IQueryable<Model.SelectList> selectListsDb;

        public SingleSelectListQuery(IQueryable<Model.SelectList> selectListsDb)
        {
            this.selectListsDb = selectListsDb;
        }

        public async Task<Model.SelectList> Execute(Guid selectListId)
        {
            return await selectListsDb.SingleAsync(list => list.Id == selectListId);
        }
    }
}
