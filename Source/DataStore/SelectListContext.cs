﻿using System.Threading.Tasks;
using Fullstack.SelectList.DataStore.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Logging;

namespace Fullstack.SelectList.DataStore
{
    public class SelectListContext : DbContext, ISelectListContext
    {

#if DEBUG
        public static readonly ILoggerFactory loggerFactory
            = LoggerFactory.Create(builder => { builder.AddConsole(); });
#endif

        public SelectListContext(DbContextOptions<SelectListContext> options)
            : base(options)
        {
        }

        public DbSet<Model.SelectList> SelectLists { get; set; } = default!;
        public DbSet<SelectListItem> SelectListItems { get; set; } = default!;

        private IDbContextTransaction? transaction;

        public void BeginTransaction()
        {
            transaction = Database.BeginTransaction();
        }

        public void Commit()
        {
            try
            {
                SaveChanges();
                transaction?.Commit();
            }
            finally
            {
                transaction?.Dispose();
            }
        }

        public void Rollback()
        {
            if (transaction != null)
            {
                try
                {
                    transaction.Rollback();
                }
                finally
                {
                    transaction.Dispose();
                }
            }
        }

        void ISelectListContext.SaveChanges()
        {
            base.SaveChanges();
        }

       public async Task SaveChangesAsync()
        {
            await base.SaveChangesAsync();
        }

       protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Model.SelectList>(entity =>
            {
                entity.HasKey(selectList => selectList.Id);
                entity.Property(selectList => selectList.IsDisabled).IsRequired();
            });

            modelBuilder.Entity<SelectListItem>(entity =>
            {
                entity.HasKey(item => item.Id);
                entity.Property(item => item.Order).IsRequired();
                entity.Property(item => item.IsDisabled).IsRequired();

                entity.HasOne<Model.SelectList>()
                    .WithMany(selectList => selectList.Items)
                    .HasForeignKey(item => item.SelectListId);
            });
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

#if DEBUG
            optionsBuilder.EnableSensitiveDataLogging();

            if (loggerFactory != null)
            {
                optionsBuilder.UseLoggerFactory(loggerFactory);
            }
#endif
        }
    }
}
