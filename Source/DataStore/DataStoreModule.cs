﻿using Autofac;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace Fullstack.SelectList.DataStore
{
    public class DataStoreModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.Register(context =>
            {
                return new DbContextOptionsBuilder<SelectListContext>()
                    .UseInMemoryDatabase("SelectList")
                    .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                    .Options;
            }).As<DbContextOptions<SelectListContext>>().IfNotRegistered(typeof(DbContextOptions<SelectListContext>));

            builder.RegisterType<SelectListContext>().As<ISelectListContext>();
        }
    }
}
