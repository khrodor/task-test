﻿using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Fullstack.SelectList.DataStore.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Fullstack.SelectList.DataStore
{
    public interface ISelectListContext
    {
        DbSet<Model.SelectList> SelectLists { get; set; }
        DbSet<SelectListItem> SelectListItems { get; set; }

        EntityEntry Entry([NotNull] object entity);

        void BeginTransaction();
        void Commit();
        void Rollback();
        void SaveChanges();
        Task SaveChangesAsync();
    }
}
