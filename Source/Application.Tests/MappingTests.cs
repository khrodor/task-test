﻿using Affecto.Mapping.AutoMapper.Autofac;
using Autofac;
using AutoMapper;
using Xunit;

namespace Fullstack.SelectList.Application.Tests
{
    public class MappingTests
    {
        private readonly MapperConfiguration mapperConfiguration;

        public MappingTests()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<Api.ApiModule>();
            builder.ConfigureAutoMapper();
            IContainer container = builder.Build();
            mapperConfiguration = container.Resolve<MapperConfiguration>();
        }

        [Fact]
        public void AutoMapperConfigurationIsValid()
        {
            mapperConfiguration.AssertConfigurationIsValid();
        }
    }
}
