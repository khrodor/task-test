﻿using System;
using System.Threading.Tasks;
using Fullstack.SelectList.Application.Commands.UpdateSelectList;
using Fullstack.SelectList.Application.Exceptions;
using Fullstack.SelectList.DataStore;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using Xunit;

namespace Fullstack.SelectList.Application.Tests.SelectLists
{
    public class UpdateSelectListCommandHandlerTests
    {
        private readonly ISelectListContext context;
        private readonly UpdateSelectListCommandHandler sut;

        public UpdateSelectListCommandHandlerTests()
        {
            context = Substitute.For<ISelectListContext>();
            sut = new UpdateSelectListCommandHandler(context);
        }

        [Fact]
        public async Task HandleSavesModifiedData()
        {
            context.SelectLists
                .FindAsync(Arg.Any<Guid>())
                .Returns(new DataStore.Model.SelectList());

            var result = await sut.Handle(GetCommand(), default);

            await context.Received().SaveChangesAsync();
            Assert.True(result.IsSuccess);
        }

        [Fact]
        public async Task HandleReturnsExceptionIfEntityDoesNotExists()
        {
            var result = await sut.Handle(GetCommand(), default);

            Assert.False(result.IsSuccess);
            Assert.IsType<EntityNotExistsException>(result.Exception);
        }

        [Fact]
        public async Task HandleReturnsErrorWhenDatabaseProviderException()
        {
            context.SelectLists
                .FindAsync(Arg.Any<Guid>())
                .Returns(new DataStore.Model.SelectList());
            context.SaveChangesAsync()
                .Throws(new MissingMethodException());

            var result = await sut.Handle(GetCommand(), default);

            Assert.False(result.IsSuccess);
            Assert.IsType<MissingMethodException>(result.Exception);
        }

        private static UpdateSelectListCommand GetCommand() => new UpdateSelectListCommand(Guid.NewGuid(), "foo", "bar");
    }
}
