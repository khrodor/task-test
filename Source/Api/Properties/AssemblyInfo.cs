using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Fullstack.SelectList.Api.Tests")]
[assembly: InternalsVisibleTo("Fullstack.SelectList.Application.Tests")]
[assembly: InternalsVisibleTo("Fullstack.SelectList.FeatureTests")]
