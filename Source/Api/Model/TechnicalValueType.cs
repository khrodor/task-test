﻿namespace Fullstack.SelectList.Api.Model
{
    /// <summary>
    /// Enumeration used to determine technical value type for select list
    /// </summary>
    public enum TechnicalValueType
    {
        /// <summary>
        /// Boolean
        /// </summary>
        Boolean,

        /// <summary>
        /// Number
        /// </summary>
        Number
    }
}
