﻿using System;

namespace Fullstack.SelectList.Api.Model
{
    /// <summary>
    /// Select list item and its properties
    /// </summary>
    public class SelectListItem
    {
        /// <summary>
        /// Select list item unique identifier
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Select list item technical name
        /// </summary>
        public string TechnicalName { get; set; }

        /// <summary>
        /// Select list item technical value
        /// </summary>
        public object TechnicalValue { get; set; }

        /// <summary>
        /// Select list item value for determining order in the list
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Value for determining is select list item disabled
        /// </summary>
        public bool IsDisabled { get; set; }
    }
}
