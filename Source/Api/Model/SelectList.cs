﻿using System;

namespace Fullstack.SelectList.Api.Model
{
    /// <summary>
    /// Select list and its properties (without items)
    /// </summary>
    public class SelectList
    {
        /// <summary>
        /// Select list unique identifier
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Select list name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Select list description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Select list technical value type
        /// </summary>
        public TechnicalValueType TechnicalValueType { get; set; }

        /// <summary>
        /// Value for determining is select list disabled
        /// </summary>
        public bool IsDisabled { get; set; }
    }
}