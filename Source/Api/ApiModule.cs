﻿using Affecto.Mapping.AutoMapper.Autofac;
using Autofac;
using Fullstack.SelectList.Application;
using Fullstack.SelectList.DataStore;
using MediatR;
using Microsoft.Extensions.Configuration;
using ApiModel = Fullstack.SelectList.Api.Model;
using OutMappers = Fullstack.SelectList.Api.Mappers.Out;

namespace Fullstack.SelectList.Api
{
    internal class ApiModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            IConfigurationRoot configurationRoot = new ConfigurationBuilder().AddJsonFile("appsettings.json", true, true).Build();
            builder.RegisterInstance(configurationRoot).As<IConfigurationRoot>();

            builder.RegisterType<Mediator>().As<IMediator>().InstancePerLifetimeScope();

            builder.ConfigureAutoMapper();
            RegisterMappers(builder);

            builder.RegisterModule<DataStoreModule>();
            builder.RegisterModule<ApplicationModule>();

            builder.Register<ServiceFactory>(ctx =>
            {
                var c = ctx.Resolve<IComponentContext>();
                return t => c.Resolve(t);
            });
        }

        private static void RegisterMappers(ContainerBuilder builder)
        {
            builder.RegisterMappingProfile<OutMappers.SelectListProfile, Application.Model.SelectList, ApiModel.SelectList>();
            builder.RegisterMappingProfile<OutMappers.SelectListItemProfile, Application.Model.SelectListItem, ApiModel.SelectListItem>();
        }
    }
}