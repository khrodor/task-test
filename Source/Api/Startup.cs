﻿using System;
using Autofac;
using Fullstack.SelectList.Api.Middleware;
using Fullstack.SelectList.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Fullstack.SelectList.Api
{
    internal class Startup
    {
        private readonly string AllowSpecificOrigins = "AllowSpecificOrigins";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var allowedOrigins = Configuration.GetSection("SelectListApi:AllowedOrigins").Get<string[]>();

            services.AddCors(options =>
            {
                options.AddPolicy(AllowSpecificOrigins, builder =>
                {
                    builder.WithOrigins(allowedOrigins)
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                });
            });

            services
                .AddControllers()
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                    options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Unspecified;
                });

            var selectListApiConfiguration = new SelectListApiConfiguration();
            IConfigurationSection selectListApiConfigurationSection = Configuration.GetSection("SelectListApi");
            selectListApiConfigurationSection.Bind(selectListApiConfiguration);

            services.AddSingleton(selectListApiConfiguration);

            services.AddSwaggerDocument(options =>
            {
                options.Title = "Select List API";
                options.Description = string.Empty;
                options.Version = "v1";
            });
        }

        public virtual void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule<ApiModule>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseUnhandledExceptionHandler();

            app.UseRouting();

            app.UseHttpsRedirection();

            app.UseCors(AllowSpecificOrigins);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseOpenApi();

            app.UseSwaggerUi3(config => config.TransformToExternalPath = (internalUiRoute, request) =>
            {
                if (internalUiRoute.StartsWith("/") && !internalUiRoute.StartsWith(request.PathBase))
                {
                    return request.PathBase + internalUiRoute;
                }

                return internalUiRoute;
            });
        }
    }
}
