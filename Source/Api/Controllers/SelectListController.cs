﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Affecto.Mapping;
using Fullstack.SelectList.Application.Queries.GetSelectLists;
using Fullstack.SelectList.Application.Queries.GetSingleSelectList;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using ApiCommand = Fullstack.SelectList.Api.Commands;
using ApiModel = Fullstack.SelectList.Api.Model;
using AppModel = Fullstack.SelectList.Application.Model;

namespace Fullstack.SelectList.Api.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("[controller]")]
    [ApiController]
    public class SelectListController : ControllerBase
    {
        private readonly IMediator mediator;
        private readonly IMapperFactory mapperFactory;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mediator"></param>
        /// <param name="mapperFactory"></param>
        public SelectListController(IMediator mediator, IMapperFactory mapperFactory)
        {
            this.mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            this.mapperFactory = mapperFactory ?? throw new ArgumentNullException(nameof(mapperFactory));
        }

        /// <summary>
        /// Get a list of all existing select lists and their properties (without items)
        /// </summary>
        /// <returns>Select lists and their properties (without items)</returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, typeof(IEnumerable<ApiModel.SelectList>), Description = "The request was successful, the response body contains information about all the select lists and their properties.")]
        public async Task<ActionResult<IEnumerable<ApiModel.SelectList>>> GetSelectLists()
        {
            GetSelectListsQuery query = new GetSelectListsQuery();
            IReadOnlyCollection<AppModel.SelectList> selectLists = await mediator.Send(query);
            IMapper<AppModel.SelectList, ApiModel.SelectList> mapper = mapperFactory.Create<AppModel.SelectList, ApiModel.SelectList>();
            IReadOnlyCollection<ApiModel.SelectList> mappedSelectLists = mapper.Map(selectLists);

            return Ok(mappedSelectLists);
        }

        /// <summary>
        /// Get given select list
        /// </summary>
        /// <param name="selectListId"></param>
        /// <returns></returns>
        [HttpGet("{selectListId}")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(ApiModel.SelectList), Description = "The request was successful, the response body contains information about specific select lists and it's properties.")]
        public async Task<ActionResult<ApiModel.SelectList>> GetSelectList(Guid selectListId)
        {
            var query = new GetSingleSelectListQuery(selectListId);
            AppModel.SelectList selectList = await mediator.Send(query);
            IMapper<AppModel.SelectList, ApiModel.SelectList> mapper = mapperFactory.Create<AppModel.SelectList, ApiModel.SelectList>();
            ApiModel.SelectList mappedSelectList = mapper.Map(selectList);

            return Ok(mappedSelectList);
        }

        /// <summary>
        /// Modify properties of an existing select list
        /// </summary>
        /// <param name="selectListId"></param>
        /// <param name="command">Select list update command</param>
        /// <returns></returns>
        [HttpPut("{selectListId}")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(ApiModel.SelectList))]
        public async Task<ActionResult<ApiModel.SelectList>> UpdateSelectList(Guid selectListId, [FromBody] ApiCommand.UpdateSelectListCommand command)
        {
            var commandRequest = new Application.Commands.UpdateSelectList.UpdateSelectListCommand(selectListId, command.Name, command.Description);
            var result = await mediator.Send(commandRequest);

            if (result.IsSuccess)
            {
                return await GetSelectList(selectListId);
            }
            if (result.Exception?.GetType() == typeof(Application.Exceptions.EntityNotExistsException))
            {
                return NotFound();
            }
            return BadRequest();
        }
    }
}