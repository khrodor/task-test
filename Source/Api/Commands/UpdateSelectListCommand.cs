﻿using Fullstack.SelectList.Api.Model;

namespace Fullstack.SelectList.Api.Commands
{
    /// <summary>
    /// Command for updating select list
    /// </summary>
    public class UpdateSelectListCommand
    {
        /// <summary>
        /// Select list name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Select list description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Select list technical value type
        /// </summary>
        public TechnicalValueType TechnicalValueType { get; set; }

        /// <summary>
        /// Value for determining is select list disabled
        /// </summary>
        public bool IsDisabled { get; set; }
    }
}
