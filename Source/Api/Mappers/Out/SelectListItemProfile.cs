﻿using Affecto.Mapping.AutoMapper;
using AutoMapper;

namespace Fullstack.SelectList.Api.Mappers.Out
{
    internal class SelectListItemProfile : MappingProfile<Application.Model.SelectListItem, Model.SelectListItem>
    {
        protected override void ConfigureMapping(IMappingExpression<Application.Model.SelectListItem, Model.SelectListItem> map)
        {

        }
    }
}