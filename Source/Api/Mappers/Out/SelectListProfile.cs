﻿using Affecto.Mapping.AutoMapper;
using AutoMapper;

namespace Fullstack.SelectList.Api.Mappers.Out
{
    internal class SelectListProfile : MappingProfile<Application.Model.SelectList, Model.SelectList>
    {
        protected override void ConfigureMapping(IMappingExpression<Application.Model.SelectList, Model.SelectList> map)
        {

        }
    }
}